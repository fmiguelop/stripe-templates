import Logo from '../src/assets/img/logo.svg'
import Image from 'next/image'
import { ArrowRightIcon, CodeIcon, CreditCardIcon, UserIcon, ChartBarIcon, MenuIcon, RefreshIcon } from '@heroicons/react/solid'
import Dev1 from '../src/assets/img/device-1.svg'
import Dev2 from '../src/assets/img/device-2.svg'
import Dev3 from '../src/assets/img/device-3.svg'
import Dev4 from '../src/assets/img/device-4.svg'
import Toggle from '../src/assets/img/toggle.svg'
import Gears from '../src/assets/img/Gears.svg'
import Code from '../src/assets/img/code.svg'
import Improve from '../src/assets/img/improve.svg'
import Scale from '../src/assets/img/scale.svg'
import Twitter from '../src/assets/img/twitter.svg'
import Instacart from '../src/assets/img/instacart.svg'
import Kickstarter from '../src/assets/img/kickstarter.svg'
import Pinterest from '../src/assets/img/pinterest.svg'
import Shopify from '../src/assets/img/shopify.svg'
import Slack from '../src/assets/img/slack.svg'
import Opentable from '../src/assets/img/opentable.svg'
import Lyft from '../src/assets/img/lyft.svg'
import Orb from '../src/assets/img/orb.svg'
import Book from '../src/assets/img/book.svg'
import Location from '../src/assets/img/location.svg'
import Message from '../src/assets/img/message.svg'

const Home = () => (

    <div id="main">
      <header>
        <div id="stripes">
          <span></span>
          <span></span>
          <span></span>
          <span></span>
          <span></span>
        </div>
        <nav className="mobile">
          <Image src={Logo} alt='stripe logo' id="logo" />
          <MenuIcon id="menu"/>
        </nav>
        <nav className="navbar">
          <Image src={Logo} alt='stripe logo' id="logo"/>
          <div className="navbar-section">
            <p>Products</p>
            <p>Developers</p>
            <p>Company</p>
          </div>
          <div className="navbar-section">
            <p>Support</p>
            <span id="sign-in">
              <p>Sign in</p>
            </span>
          </div>
        </nav>
      </header>
      <section id="intro">
          <div className="container">
            <p className="announcement">
              <span id="pill">New</span>
              <span id="message">Introducing Radar: Modern tools to help you beat fraud, fully integrated with your payments.</span>
            </p>
            <h1>The new standard in online payments</h1>
            <p className="description">
              Stripe is the best software platform for running an internet
              business. We handle billions of dollars every year for forward-
              thinking businesses around the world.
            </p>
            <ul>
              <li className='button'>
                <CodeIcon id="buttons-icon" />
                EXPLORE THE STACK
              </li>
              <li className='button'>
                CREATE ACCOUNT
              </li>
            </ul>
          </div>
      </section>
      <section id="images">
        <div className="tablet-landscape">
          <Image src={Dev1} alt='dev1'/>
        </div>
        <div className="phone-big">
          <Image src={Dev2} alt='dev2'/>
        </div>
        <div className="phone-small">
          <Image src={Dev3} alt='dev3'/>
        </div>
        <div className="tablet-portrait">
          <Image src={Dev4} alt='dev4'/>
        </div>
      </section>
      <section id="primary">
        <section id="toolkit" className="container">
          <h2 className="title">
            <Image src={Toggle} alt="toggle" className='head-icon'/>
            <span>THE COMPLETE TOOLKIT FOR
              INTERNET BUSINESS</span>
          </h2>
          <p className="body-text">
            Stripe builds the most powerful and flexible tools for internet commerce. Whether you’re creating a subscription service, an on-demand marketplace, an e-commerce store, or a crowdfunding platform, Stripe’s meticulously-designed APIs and unmatched functionality help you create the best possible product for your users. Hundreds of thousands of the world’s most innovative technology companies are scaling faster and more efficiently by building their businesses on Stripe.
          </p>
          <p className='linked-arrow'>
            Discover how businesses use Stripe
            <ArrowRightIcon id="icon"/>
          </p>
        </section>
        <section id="dev-first">
          <h2 className="title">
            <Image src={Gears} alt="toggle" className='head-icon' />
            <span id="title-head">DEVELOPERS FIRST</span>
          </h2>
          <p className="body-text">
            We believe that payments is a problem rooted in code, not finance. We obsessively seek out elegant, composable abstractions that enable robust, scalable, flexible integrations. Because we eliminate needless complexity and extraneous details, you can get up and running with Stripe in just a couple of minutes.
          </p>
        </section>
        <section id="notebook" className="container-lg">
          <nav>
            <select>
              <option value="payments">Payments</option>
              <option value="customers">Customers</option>
              <option value="subscriptions">Subscriptions</option>
              <option value="reporting">Reporting</option>
            </select>
            <ul >
              <li>
                <span className='active-button'>
                  <CreditCardIcon id="icon"/>
                  <p>Payments</p>
                </span>
              </li>
              <li>
                <span className='inactive-button'>
                  <UserIcon id="icon"/>
                  <p>Customers</p>
                </span>
              </li>
              <li>
                <span className='inactive-button'>
                  <RefreshIcon id="icon"/>
                  <p>Subscriptions</p>
                </span>
              </li>
              <li>
                <span className='inactive-button'>
                  <ChartBarIcon id="icon"/>
                  <p>Reporting</p>
                </span>
              </li>
            </ul>
            <p className="linked-arrow">Full API reference</p>
          </nav>
          <div id="code">
            <Image src={Code} alt="code"/>
          </div>
        </section>
      </section>
      <section id="secondary">
        <div className="cols container-lg">
          <section id="improving">
            <h2>
              <Image src={Improve} alt="improve"/>
              <p>Always improving</p>
            </h2>
            <p className="body-text">
              Stripe is an always-improving toolchain that gains new features every month. Our world-class engineering team constantly iterates upon every facet of the Stripe stack. And from Apple Pay to Bitcoin, building on Stripe means you get early access to the latest technologies.
            </p>
            <p className="linked-arrow">
              Learn about Stripe’s products
              <ArrowRightIcon id="icon"/>
            </p>
          </section>
          <section id="global-scale">
            <h2>
              <Image src={Scale} alt="improve"/>
              <p>GLOBAL SCALE</p>
            </h2>
            <p className="body-text">
              We help power 100,000+ businesses in 100+ countries and across nearly every industry. Headquartered in San Francisco, Stripe has 9 global offices and hundreds of people working to help transform how modern businesses are built and run.
            </p>
            <p className="linked-arrow">
              More about us
              <ArrowRightIcon id="icon"/>
            </p>
          </section>
        </div>
          <section id="logos">
            <ul>
              <li>
                <Image src={Kickstarter} alt="kickstarter" className="kickstarter" />
              </li>
              <li>
                <Image src={Twitter} alt="Twitter" className="Twitter" />
              </li>
              <li>
                <Image src={Instacart} alt="Instacart" className="Instacart" />
              </li>
              <li>
                <Image src={Pinterest} alt="Pinterest" className="Pinterest" />
              </li>
              <li>
                <Image src={Lyft} alt="Lyft" className="Lyft" />
              </li>
              <li>
                <Image src={Shopify} alt="Shopify" className="Shopify" />
              </li>
              <li>
                <Image src={Opentable} alt="Opentable" className="Opentable" />
              </li>
              <li>
                <Image src={Slack} alt="Slack" className="Slack" />
              </li>
            </ul>
          </section>
      </section>
      <div>
        <section className='footer-cards'>
          <div className="container-xl">
            <div className="card">
              <Image src={Orb} alt="orb"/>
              <span id="card-body">
                <h2>INTRODUCING RADAR</h2>
                <p className='body-text'>
                  Use advanced machine learning that learns from 100,000+ global businesses to help your company beat fraud.
                </p>
              </span>
            </div>
            <div className="card">
              <Image src={Book} alt='book'/>
              <span id="card-body">
                <h2>EXPLORE THE DOCS</h2>
                <p className='body-text'>
                  Use advanced machine learning that learns from 100,000+ global businesses to help your company beat fraud.
                </p>
              </span>
            </div>
          </div>
        </section>
        <article className='contact-section'>
          <div className="container-lg">
            <div className="content">
              <h1 className="title">
                <span className="subtitle">Ready to get started?</span>
                Get in touch, or create an account.
              </h1>
            </div>
            <div className="buttons">
              <span className="button">CREATE STRIPE ACCOUNT</span>
              <span className="button">CONTACT SALES</span>
            </div>
          </div>
        </article>
        <article className="footer-nav">
          <div className="container-lg">
            <ul className="meta-nav">
              <li  id="location">
                <p>
                  <Image src={Location} alt='location' className='icon'/>
                  <span>United States</span>
                </p>
              </li>
              <li>
                <p>
                  <Image src={Message} alt='location' className='icon'/>
                  <span>English</span>
                </p>
              </li>
              <li className="space"></li>
              <li>
                © 2020 Stripe
              </li>
            </ul>
            <div className="site-nav">
              <div className="column">
                <h4>Products</h4>
                <div className="split">
                  <ul>
                    <li>Payments</li>
                    <li>Subscription</li>
                    <li>Connect</li>
                    <li>Relay</li>
                    <li>Atlas</li>
                  </ul>
                  <ul>
                    <li>Pricing</li>
                    <li>Customers</li>
                    <li>Global</li>
                  </ul>
                </div>
              </div>
              <div className="column">
                <h4>COMPANY</h4>
                <ul>
                  <li>About</li>
                  <li>Blog</li>
                  <li>Jobs</li>
                  <li>Press</li>
                </ul>
              </div>
              <div className="column">
                <h4>Developers</h4>
                <ul>
                  <li>Documentation</li>
                  <li>API reference</li>
                  <li>API status</li>
                  <li>Open Source</li>
                </ul>
              </div>
              <div className="column">
                <h4>RESOURCES</h4>
                <ul>
                  <li>Support</li>
                  <li>Contact</li>
                  <li>Privacy & terms</li>
                </ul>
              </div>
            </div>
          </div>
        </article>
      </div>
    </div> 
)

export default Home;