module.exports = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      fontFamily: {
      },
      rotate: {
        '30': '30deg',
        '15': '15deg',
      }
    },
  },
  plugins: [],
}
